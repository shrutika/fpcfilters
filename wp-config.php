<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fpcfilters');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NMRD4nj{#]pjsQ!qWg17?4)DFD8teLRO|#2o4T!*eb88O45(r]5ZYwVml,+Y6ssp');
define('SECURE_AUTH_KEY',  '[i^mn={;JYnQouP0QyC&V~iC:I5SgoghhG?I0wZNi^jIbx}D59$5AZQJ/sAA&2:_');
define('LOGGED_IN_KEY',    '?_~,5T v cI3k|k~Wzx4WavsWioM<dLEF4;aoE55%*{m*:%~3OH2vau!OVAm|$*X');
define('NONCE_KEY',        'W9LAkfyZMj^>4Pt4@gBWx{SitAtR|BhNJ#.lc%:T.z|>o]I8d:fU]4m3_kFD?`ry');
define('AUTH_SALT',        'T!{B>SOz[XBPyX]PaHjDc<-xJCUrW,wBq}#W~JsLrq]?dyKpm}.f(y<FabodDW8@');
define('SECURE_AUTH_SALT', ' <Q,H]x>T<r:5XMmyH)d2o;YwO#KIYN$%N<M0W6lvK?X#v`V9SMO{UA%<k070c3B');
define('LOGGED_IN_SALT',   'uoqp=gRUuR!uTG3X4IrQpDK[6>Ca#6-4eI^%bVE-~yDb,hPn8bj2B6+`epmJMobx');
define('NONCE_SALT',       'MY*F/+HQsbhrP= (:6&5;DD_Io5bJQ$zq<tf7%`OG)UpfSjr9N #Bepy+J~CY(vN');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
