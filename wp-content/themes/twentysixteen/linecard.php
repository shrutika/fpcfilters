<?php  /*template Name: linecard */ ?>
<?php get_header(); ?>

<div class="container">
  
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Our Line Card</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body"><?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
           
           <p> <?php the_content();?> </p>
            <?php endwhile; ?> 
            <?php endif; ?></div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Featured Manufacturers</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body"><?php get_template_part( 'products' ); ?></div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Filter Applications</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">

  
<?php get_template_part( 'applications' ); ?></div>
      </div>
    </div>
  </div> 
</div>
<?php get_footer(); ?>
    

